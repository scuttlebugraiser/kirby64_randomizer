from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from pathlib import Path
import sys, random
import randomizer


class Window(QWidget):
	def __init__(self, file):
		super().__init__()
		self.resize(400, 200) #idk
		self.setWindowTitle("Kirby Crystal Shards Randomizer")
		self.setAcceptDrops(True)
		font = QFont()
		font.setPointSize(11)
		self.setFont(font)
		self.layout = QVBoxLayout(self)
		self.setLayout(self.layout)
		self.use_seed = 0
		if file:
			self.rom = file
	def AddLayout(self, LY, layout = None):
		if layout:
			layout.addLayout(LY)
		else:
			self.layout.addLayout(LY)
		return LY
	def AddWidget(self, widg, LY = None, **kwargs):
		if LY == None:
			self.layout.addWidget(widg, *kwargs.values())
		else:
			LY.addWidget(widg, *kwargs.values())
	def dragEnterEvent(self,e):
		print("drag event")
		if e.mimeData().hasText():
			if 'file' in e.mimeData().text() and ('.z64' in e.mimeData().text()):
				e.accept()
		else:
			e.ignore()
	def dropEvent(self, e):
		self.rom = Path(e.mimeData().text()[8:])
		self.UpdateStatus(f"ROM accepted, {self.rom.name} is ready to randomize")
	def ToggleSeed(self, state):
		if state:
			self.Seed.show()
			self.GenSeed.show()
			self.use_seed = 1
		else:
			self.Seed.hide()
			self.GenSeed.hide()
			self.use_seed = 0
	def NewSeed(self):
		self.Seed.setText( str(random.randrange(0, 0xFFFFFFFF)) )
	def Randomize(self):
		if not self.rom:
			self.UpdateStatus("No rom found, cannot randomize. Drop in Valid .z64 file")
		else:
			if self.use_seed:
				seed = self.Seed.text()
			else:
				seed = None
			seed = randomizer.Main(self.rom, Seed = seed,
					SpeedEdit = self.ArgSend(self.Spd.isChecked()),
					NoHealth = self.ArgSend(self.NoHP.isChecked()),
					HardMode = self.ArgSend(self.Hard.isChecked()),
					ScramMusic = self.ArgSend(self.Music.isChecked())
				)
			self.UpdateStatus(f"{self.rom.name} randomized, {self.rom.stem}_rand.z64 created.")
	def UpdateStatus(self, s):
		self.status.setText(s)
	#if condition, then send condition, else send None
	#(default would be 0/False etc., but I want None specifically)
	def ArgSend(self, cond):
		if cond:
			return cond
		return None

#style sheets are basically CSS, aka evil
def EmbossStyleSheet(widget):
	style = """
	background-color: #dee5da;
	border: 2px solid black;
	padding: 2px;
	"""
	widget.setStyleSheet(style)
	font = QFont()
	font.setPointSize(11)
	widget.setFont(font)
	widget.setAlignment(Qt.AlignCenter)

def BoldStyleSheet(widget):
	style = """
	background-color: #ebefdc;
	border: 1px dotted black;
	margin: 3px;
	padding: 4px;
	"""
	widget.setStyleSheet(style)
	font = QFont()
	font.setPointSize(11)
	font.setBold(True)
	widget.setFont(font)

def InitGui(file):
	#create window
	app = QApplication([])
	wnd = Window(file)
	#add widgets
	Randomize = QPushButton("Randomize Rom")
	#status
	if file:
		sts = f"ROM is {file.name}. Ready to randomize"
	else:
		sts = "No rom detected. Drop in valid .z64 file"
	Status = QLabel(sts)
	wnd.status = Status
	Status.setFixedHeight(30)
	#seed
	UseSeed = QCheckBox("Use RNG Seed")
	Seed = QLineEdit("0")
	Seed.hide()
	wnd.Seed = Seed
	GenSeed = QPushButton("Generate Seed")
	GenSeed.hide()
	wnd.GenSeed = GenSeed
	#various options
	Hard = QCheckBox("Hard Mode")
	Spd = QCheckBox("Randomize Speed")
	Music = QCheckBox("Scramble Music")
	NoHP = QCheckBox("Remove Health Items")
	
	wnd.Hard = Hard
	wnd.Spd = Spd
	wnd.Music = Music
	wnd.NoHP = NoHP
	#add functions
	Randomize.clicked.connect( wnd.Randomize )
	GenSeed.clicked.connect( wnd.NewSeed )
	UseSeed.stateChanged.connect( wnd.ToggleSeed )
	
	#styles
	EmbossStyleSheet(Status)
	BoldStyleSheet(Hard)
	BoldStyleSheet(Spd)
	BoldStyleSheet(Music)
	BoldStyleSheet(NoHP)
	
	#add widgets to layout
	sts = QHBoxLayout()
	top = QHBoxLayout()
	options = QGridLayout()
	bot = QHBoxLayout()
	
	wnd.AddLayout(sts)
	wnd.AddLayout(top)
	wnd.AddLayout(options)
	wnd.AddLayout(bot)
	
	wnd.AddWidget(Status, LY = sts)
	wnd.AddWidget(Randomize, LY = bot)
	#kwargs don't work for add layout, they need to be in order
	#I unroll them in the class, and just use the naming so it
	#is clear what is going on
	# wnd.AddWidget(Hard, LY = options, row = 0, column = 0)
	# wnd.AddWidget(Spd, LY = options, row = 1, column = 0)
	wnd.AddWidget(Music, LY = options, row = 1, column = 1)
	wnd.AddWidget(NoHP, LY = options, row = 0, column = 1)
	
	wnd.AddWidget(UseSeed, LY = top, stretch = 1)
	wnd.AddWidget(Seed, LY = top, stretch = 3) #hiden by default
	wnd.AddWidget(GenSeed, LY = top, stretch = 2) #hiden by default
	return wnd, app

def RunGui(window, app):
	window.show()
	sys.exit(app.exec_())

def Main():
	#get default kirby rom
	Kirb = Path("Kirby64.z64")
	if Kirb.exists():
		file = Kirb
	else:
		file = None
	#make GUI
	wnd, app = InitGui(file)
	#run GUI
	RunGui(wnd, app)

if __name__== '__main__':
	Main()