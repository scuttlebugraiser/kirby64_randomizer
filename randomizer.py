#kirby randomizer 2.0
#randomizes warps, levels, enemies
#of similar types and music
from pathlib import Path
import struct, subprocess, sys, copy
from dataclasses import dataclass
from collections import namedtuple
from random import *

HS = 0x80057DB0

MainStageStruct={
	0x00:['geo_block_a','>2H',4],
	0x04:['geo_block_b','>2H',4],
	0x08:['skybox_id','>H',2],
	0x0A:['color','>H',2],
	0x0C:['music_id','>L',4],
	0x10:['level_block','>2H',4],
	0x14:['Death Cutscene','>H',2],
	0x16:['Cutsecene related','>H',2],
	0x18:['dust_block','>2H',4],
	0x1C:['dust_img','>2H',4],
	0x20:['area_name','>L',4],
}

class Log():
	def __init__(self):
		self.log = open("log.txt", "w")
	def Write(self, text):
		self.log.write(text)
		self.log.write("\n\n")

@dataclass
class Stage():
	skybox : int
	music : int
	geo: tuple
	geo2: tuple
	color: int
	cutscene: int
	lvl_type: int
	dust: tuple
	dust_img: tuple
	name: int
	col: tuple
	world: int
	level: int
	area: int
	ptr: int
	def copy(self, stage, rom, wr_rom):
		wr_rom.seek(self.ptr)
		wr_rom.write(rom[stage.ptr:stage.ptr+0x24])
	def scram_music(self):
		while(True):
			new = randint(1, 0x3E)
			#dont pick bad music
			if new not in (0, 3, 4, 5, 9, 0x17, 0x18, 0x1F, 0x26, *range(0x2A,0x37,1), 0x38, 0x39, 0x3a):
				break
		self.music = (new,)

#queue stage prop editing for stages not in existance yet
q = {}
def Queue(area, exit):
	global q
	q[area] = exit

def ChkQ(area):
	global q
	return q.get(area)

#boss levels and level ends do not have warps
class Level():
	def __init__(self, block, stage, stageDict, ptr):
		self.block = block #class with bin data
		self.stage = stage #stage class
		self.ptr = ptr
		self.warpsF = {}
		self.warpsB = {}
		self.exit = None
		self.SD = stageDict
		self.area = (stage.world, stage.level, stage.area)
		self.entrance = ChkQ(self.area)
		self.areanew = self.area
		self.GetWarps(stageDict)
		stageDict[self.area] = self
	def GetWarps(self,sd):
		CurArea = self.area[-1]
		for num,node in self.block.path_nodes.items():
			kn = node.kirb_node
			if kn.WarpFlag & 1:
				if kn.a > CurArea:
					self.warpsF[num] = kn[2:6]
					self.exit = num
					try:
						self.SD[kn[2:5]].entrance = num
					except:
						Queue(kn[2:5], num)
				else:
					self.warpsB[num] = kn[2:6]
				#print(f'n {num} warp {kn[2:6]} backwards {kn.backwards} area {self.area}')
		#you can tell what type of level it is by using the 'cutscene' flag in the stage struct
		#this was previously identified wrong, look at notes to verify stage type flags
		lvl_type = self.stage.lvl_type[0]
		if lvl_type == 3:
			sd["carpet"].append(self) #carpet
			self.tag = 'carpet'
		elif lvl_type == 2:
			sd["boss"].append(self) #boss
			self.tag = 'boss'
		elif lvl_type > 3 and lvl_type < 7:
			sd['waddleD'].append(self) #hardcoded warp
			self.tag = 'waddleD'
		elif lvl_type == 8:
			sd['DDD'].append(self) #hardcoded warp
			self.tag = 'DDD'
		else:
			if CurArea == 0:
				sd["first"].append(self) #first lvl
				self.tag = 'first'
			else:
				sd["lvl"].append(self) #lvl
				self.tag = 'lvl'

#stages I hardcode to not have backwarps, these have level
#specific events so you can't go back or it'll break
NoBacks = [(1, 0, 3), (1, 3, 0), (1, 3, 1), (3, 3, 5), (3, 3, 6), (5, 1, 5),
(4, 1, 0), (3, 1, 0)]


def SetWarp(src, dest, rom, read, sd, slot):
	#set attributes for debugging
	if not hasattr(src, 'set_warpF'):
		src.set_warpF = None
	if not hasattr(dest, 'set_warpF'):
		dest.set_warpF = None
	if not hasattr(src, 'set_warpB'):
		src.set_warpB = None
	if not hasattr(dest, 'set_warpB'):
		dest.set_warpB = None
	#set  src level warps to dest
	for num, warp in src.warpsF.items():
		pn = src.block.path_nodes[num].path_node
		ptr = pn[0] + src.ptr + 4 #ptr to warp in ROM
		#chck = struct.unpack(">4B", read[ptr:ptr+4])
		#check where you land from prev level
		#no warps forward exist in waddle D lvls
		#set all exiting warps to be one entrance.
		#may attempt to set multi exits later
		entrance = sd[(*dest.area[0:2], dest.stage.area-1)]
		Fslot = (*slot[0:2], slot[2]+1)
		if entrance.warpsF:
			new = (*Fslot, list(entrance.warpsF.values())[0][-1])
			#this is a hardcoded warp present in the ado fight that doesn't work
			#occurs in some warps to carpet stages (a flag of some sort possibly?)
			if new[-1] == 31:
				new = (*Fslot, 0)
		else:
			#usually 1 is the entrance warp
			new = (*Fslot, 1)
		src.set_warpF = new
		new = struct.pack(">4B", *new)
		rom.seek(ptr)
		rom.write(new)
	#write backwards warps
	#if first area, back dest is now guess
	for num, warp in dest.warpsB.items():
		pn = dest.block.path_nodes[num].path_node
		ptr = pn[0] + dest.ptr + 4
		entrance = sd[(*src.area[0:2], src.stage.area+1)]
		#if no warpB, then the src area usually leads to a one way path
		#this is stuff like bosses / cutscene interactions (DDD/waddle D usually)
		#and also carpet stages
		if entrance.warpsB:
			newB = (*slot, list(entrance.warpsB.values())[0][-1])
		else:
			#usually 2 is the exit warp
			newB = (*slot, 2)
			if src.area in NoBacks:
				#disable the warp in these instances, because going back crashes the game
				#I don't know why, or how to detect this behavior but it seems to be caused
				#by level specific events sometimes (one ex. is the room filling with sand)
				rom.seek(ptr+10)
				rom.write(struct.pack(">H",0))
			else:
				try:
					# print(src.area, src.areanew, f"0x{src.block.path_nodes[2].kirb_node.backwards:X}")
					rom.seek(src.block.path_nodes[2].path_node[0] + src.ptr + 2)
					rom.write( struct.pack(">2B", *(2, 1)) )
				except:
					pass
		if newB:
			dest.set_warpB = newB
			newB = struct.pack(">4B",*newB)
			rom.seek(ptr)
			rom.write(newB)
		else:
			#disable warp
			rom.seek(ptr+10)
			rom.write(struct.pack(">H",0))

#For each Level, there are n entrances and m exits
#to randomize, each entrance will be swapped
#for another, and each exit for another.
#the exit and entrance must be continuous,
#so the swaps will happen in pairs

def GetPointers(bank,id,type,Kirb):
	UPW = (lambda x: struct.unpack(">L",x)[0])
	BP = 0x000783D4
	BPs = UPW(Kirb[BP+bank*4:BP+bank*4+4])-HS
	types = {
	"Geo_Block": 0,
	"Image":2,
	"Anim":4,
	"Misc":6
	}
	off= types[type]*4
	if type=="Geo_Block":
		scale=2
	else:
		scale=1
	Table = UPW(Kirb[BPs+off:BPs+off+4])-HS
	Offset = UPW(Kirb[BPs+off+4:BPs+off+8])
	if Offset == 0xFFFFFFFF:
		Offset = 0
	Start = UPW(Kirb[Table+id*4*scale:Table+id*4*scale+4])+Offset
	End = UPW(Kirb[Table+4+id*4*scale:Table+id*4*scale+8])+Offset
	return [Start,End]

def GetStages(Kirb):
	UPW = (lambda x: struct.unpack(">L",x)[0])
	Stages = {}
	start = 0x7a1e8
	for World in range(0, 6, 1):
		Level = 0
		while(True):
			stage_start = start + Level*4 + World*48
			ptr = UPW(Kirb[stage_start : stage_start+4])
			if ptr == 0:
				break
			ptr -= HS
			Area = 0
			while(True):
				ms = {}
				for a in MainStageStruct.items():
					value = struct.unpack(a[1][1], Kirb[ptr + Area*0x24 + a[0] : ptr + Area*0x24 + a[1][2] + a[0]])
					ms[a[0]] = value
				if ms[0] == (0, 0):
					break
				else:
					Stages[(World,Level,Area)] = Stage(
						skybox = ms[8], music = ms[12],
						color = ms[10], cutscene = ms[0x14],
						dust = ms[0x18], dust_img = ms[0x1C],
						geo = ms[0], geo2 = ms[4], col = ms[16],
						world = World, area = Area, level= Level,
						ptr = ptr + Area * 0x24,
						name = ms[0x20],
						lvl_type = ms[0x16]
					)
				Area += 1
			Level += 1
	return Stages

def WriteStageTable(sd, src_rom, rom):
	UPW = (lambda x: struct.unpack(">L",x)[0])
	PW = (lambda x, y: struct.pack(x, *y))
	start = 0x7a1e8
	last = -1
	cnt = -1
	first = UPW(src_rom[start : start+4])
	for k, v in sd.items():
		w, l, a = k
		ptr = first + (cnt * 0x24)
		#every new level, write a pointer
		if l != last:
			last = l
			stage_start = start + l*4 + w*48
			#add gap for level, but not on the first time
			if cnt > 0:
				rom.seek(ptr-HS)
				rom.write(bytes(0x24))
			cnt += 1
			ptr = first + (cnt * 0x24)
			rom.seek(stage_start)
			rom.write( PW('>L', (ptr,)) )
		rom.seek(ptr-HS)
		w = PW( (">6HL8HL"),
		(*v.geo, *v.geo2, *v.skybox, *v.color, *v.music, *v.col,
		*v.cutscene, *v.lvl_type, *v.dust, *v.dust_img, *v.name))
		rom.write(w)
		cnt += 1

#copied from decomp bpy plugin
class misc_bin():
	_vert_sig = (0x270f,0x270f,0x270f)
	def __new__(self,file):
		self.file = file
		sig_obj = self.upt(self,4,">3H",6)
		if sig_obj == self._vert_sig:
			feature = 'obj'
		else:
			sig_obj = self.upt(self,16,">3H",6)
			if sig_obj == self._vert_sig:
				feature = 'level'
			else:
				feature = "particle"
		subclass_map = {subclass.feature: subclass for subclass in self.__subclasses__()}
		subclass = subclass_map[feature]
		instance = super(misc_bin, subclass).__new__(subclass)
		return instance
	def upt(self,offset,type,len):
		return struct.unpack(type,self.file[offset:offset+len])

#for i/o of node data
class node_cls():
	def __init__(self,num):
		self.index = num
	def print(self):
		return (self.index,self.kirb_node)

#for i/o of level bin data
class level_bin(misc_bin):
	feature = 'level'
	def __init__(self,file):
		self.main_header = self.upt(0,">3L",12)
		self.col_header = self.upt(self.main_header[0],">17L",68)
		self.node_header = self.upt(self.main_header[1],">4L",16)
		self.num_nodes = self.node_header[0]
		self.path_nodes = {}
		for i in range(self.num_nodes):
			self.decode_node(i)
		self.entities = []
		x = 0
		start = self.main_header[2]
		entity = namedtuple("entity", "node bank id action res_flag spawn_flag eep pos rot scale")
		vec3f = namedtuple('vec3', 'x y z')
		if start:
			while(True):
				sig = self.upt(start+x*0x2c,">L",4)
				if sig[0] == 0x99999999:
					break
				ent = self.upt(start+x*0x2c,">6BH9f",0x2c)
				pos, rot, scale = [vec3f._make(ent[3*i + 7 : 3*i + 10]) for i in range(3)]
				self.entities.append(entity._make([*ent[:7], pos, rot, scale]))
				x += 1
	def decode_node(self,num):
		node = node_cls(num)
		kirb = namedtuple('kirb_node','node backwards w l a DestNode unused shad1 shad2 shad3 unused2 WarpFlag opt1 opt2 opt3 opt4 unused3')
		cam = namedtuple('cam_node',"""Profile pad LockX LockY LockZ pad PanHiLo PanLo PanYaw pad pad FocusX FocusY FocusZ
		NearClip FarClip CamR1 CamR2 CamYaw1 CamYaw2 CamRadius1 CamRadius2 FOV1 FOV2 CamPhi1 CamPhi2
		CamXLock1 CamXLock2 CamYLock1 CamYLock2 CamZLock1 CamZLock2 CamYawLock1 CamYawLock2 CamPitchLock1 CamPitchLock2""",rename=True)
		path_node = self.node_header[1] + (num*16)
		node.path_node = self.upt(path_node,">3L2H",16)
		node.kirb_node = kirb._make(self.upt(node.path_node[0],">2H4B4B4H2fL",0x20))
		node.cam_node = cam._make(self.upt(node.path_node[0]+0x20,">10BH25f",0x70))
		node.path_footer = self.upt(node.path_node[1],">2HfLf2L",0x18)
		node.num_connections = node.path_node[3]
		node.node_connections = self.upt(node.path_node[2],">%dB"%(0x4*node.num_connections),0x4*node.num_connections)
		node.is_loop = node.path_node[4]
		self.path_nodes[num] = node
		node.path_matrix = [self.upt(node.path_footer[3]+12*i,">3f",12) for i in range(node.path_footer[1])]

#use a gamma variation, but cap min and max, and shift it by .5 so it sits near one
def Gamma():
	r = gammavariate(5, .4) + 0.85
	r = min(3.85, max(0.95, r))
	return r

#randomize the speed by multiplying the node length values in each level
#by some amount. Try to create a "fun" distribution
def EditSpeed(rom, levels):
	#create a speed table where the dev strings are
	spd_edits = dict()
	def pf(rom, flo, start):
		rom.seek(start)
		rom.write(struct.pack(">f", flo))
	#iteration.... whoaaa....
	for j, lvl in enumerate(levels):
		for area in lvl:
			start = area.ptr
			spd_edits[ area.areanew ] = Gamma()
	#store spd table in rom starting at dev strings 0x7e050 - 0x800D5E00
	start = 0x7e050
	for area, spd in spd_edits.items():
		#formula is start + 4*area + 10*lvl * 48*world
		off = area[2]*4 + area[1]*10 + area[0]*48
		pf(rom, spd, start + off)
		
	#add in code patch for speed randomizing
	subprocess.call(['armips','spd_edit.asm'])

#for removing HP items
def SwapThree(ent):
	HP = [
		0, #maxim
		1, #sandwhich
		2, #cake
		3, #steak
		4, #ice cream
		5, #invincible candy
	]
	if ent.id in HP:
		return (6, 0) #yellow star
	return None

#dicts of valid swaps
def ValidSwapZero(ent):
	#ID: actions
	Valid_Grounds = {
		0: (0, 2), #N-Z
		1: (1, 2), #rocky (walking rock)
		3: (0, 1), #skud (walking rocket)
		7: (0, 1), #bouncy
		#8: (0, 1, 2), #glunks have issues if they are underwater
		10: (0, 1), #chilly
		13: (0, 1), #mahall (sewer hat guy)
		14: (0, 1, 2), #poppy bros
		18: (0, 1), #kany
		20: (0,), #sir kibble
		21: (0,), #gabon (skull wearing guy)
		22: (0,), #mariel
		26: (0,), #bonehead (flying skull)
		28: (0,), #bobo
		30: (0,), #punc
		31: (0, 1), #mite
		32: (0,), #sandman
		33: (0,), #flopper
		34: (0,), #kapar
		39: (0,), #tick (needle slime
		40: (0, 1), #cairn
		42: (0,), #pompey
		43: (0,), #hack
		47: (0, 1), #sawyer
		49: (0, 1), #plug
		50: (0,), #ghost knight
		52: (0, 1), #kakti
		54: (0,), #cha cha
		55: (0,), #galbo
		58: (0, 1, 2), #Nurff (boar)
		59: (0, ), #Emp (penguin)
		61: (0, ), #yariko (penguin)
		65: (0, ), #sparky (elec slime)
	}
	#air enemies
	Valid_Air = {
		2: (0, 1, 2, 3), #bronto burt
		4: (0, 1, 2), #gordo
		6: (0, 1), #spark-i
		11: (0, 1), #propeller
		23: (0,), #Large I3
		29: (0,), #bo
		44: (0, 1), #burnis (fire bird)
		45: (0, 1), #fishbone
		46: (0, 1, 3), #frigis (ice bird)
		51: (0,), #zoos
		56: (0,), #bumber
		57: (0, 1), #scarfy
		67: (0, ), #flora
		69: (0, 1, 2), #pteran
	}
	#go up (and sometimes back down) to attack
	Valid_Below = {
		2: (1,), #bronto burt
		4: (1, 4), #gordo
		48: (0,), #turbite (elec spike thing)
		35: (1,), #maw
		60: (0,), #magoo
		72: (1, 3), #mopoo
	}
	#must be on ceiling
	Valid_Ceiling = {
		23: (0,), #Large I3
		35: (0,), #maw
		36: (1,), #drop (slime)
		71: (0, 1, 2, 3), #pupa (spider)
		72: (0, 2, 4), #mopoo
	}
	#must face left
	Valid_Facing = {
		5: (0, 1, 2, 5), #shotzo
		4: (0, 2), #gordo
		16: (0, 3), #splinter
		53: (0,), #rockn (spike torpedo)
		68: (1,), #Putt
		77: (0,) #giant shotzo
	}
	#Water
	#some normal enemies have flags == 1 that show they are underwater
	Valid_Water = {
		8: (0, 1, 2), #glunks
		9: (0,), #slushy
		18: (0, 1), #kany
		27: (0, 1), #squibbly
		33: (0, 1, 2), #flopper
		45: (0,) #fishbone
	}
	#sits in wall
	Valid_Wall = {
		12: (0,), #glom
		17: (0,), #gobblin
		38: (0,) #noo
	}
	Res_1 = {
		77: (0,), #giant shotzo
		63: (0,), #wall shotzo
		5: (0,), #regular shotzo
		2: (4,), #Bronto Burt
		#ones not seen in vanilla kirby
		38: (0,), #noo
		45: (0,), #fishbone, needs to be rotated?
		#these don't do dmg
		64: (0,),
		33: (1,),
	}
	#these seem to be BG elements
	#there aren't many enemies that can attack from far away besides for shotzos
	#I added some I believe would work well for this on top of the ones seen in vanilla
	if ent.res_flag & 1:
		return RandEnt(ent, Res_1)
	
	
	#flag of 1 means that certain enemies can be underwater
	#these entities can also be used underwater, but only
	#swap if they have flag 1 and are in this list
	Flag1 = [45, 48, 25]
	
	if ent.spawn_flag & 1 and ent.id in Flag1:
		#sometimes just get a normal water ent
		if random() > .4:
			return RandEnt(ent, Valid_Water)
		return (choice(Flag1), 0)

	def Valid(ent, d):
		if ent.id in d.keys():
			return ent.action in d.get(ent.id)
		return False

	#check ground, water, air, wall then left
	if Valid(ent, Valid_Grounds):
		return RandEnt(ent, Valid_Grounds)
	
	if Valid(ent, Valid_Ceiling):
		return RandEnt(ent, Valid_Ceiling)
	
	if Valid(ent, Valid_Water):
		return RandEnt(ent, Valid_Water)
	
	if Valid(ent, Valid_Air):
		return RandEnt(ent, Valid_Air)
	
	if Valid(ent, Valid_Wall):
		return RandEnt(ent, Valid_Wall)
	
	if Valid(ent, Valid_Facing):
		return RandEnt(ent, Valid_Facing)
	
	if Valid(ent, Valid_Below):
		return RandEnt(ent, Valid_Below)
	
	return None

def RandEnt(ent, dict):
	new = choice(list(dict.keys()))
	return (new, choice(list(dict[new])))

def ScrambleEnts(rom, levels, log, NoHealth):
	def p2b(rom, half, start):
		rom.seek(start)
		rom.write(struct.pack(">2B", *half))
	#make sure each level has 3 shards only
	for j, lvl in enumerate(levels):
		shards = []
		potentials = []
		randoms = [] #randomly picked ents to swap into crystal shards
		#I won't do col detection algs or anything to find a good place to put these
		for area in lvl:
			if area.tag == 'carpet' or area.tag == 'boss':
				continue
			entities = area.block.entities
			start = area.ptr + area.block.main_header[2]
			for k, ent in enumerate(entities):
				
				#replace stuff with things in same pool
				if ent.bank == 0:
					if ent.id == 73: #zebon, green slime, necessary for progression
						continue
					new = ValidSwapZero(ent)
					if new:
						log.Write(f"  wrote ent {new} to {area.area}")
						p2b(rom, new, (start + 0x2c *k + 2))
					#add ents randomly to randoms in case I need to update them to a crystal shard
					if random() > .95:
						randoms.append((area, ent, k))
				#solid objects
				if ent.bank == 5:
					pass
				#consumables
				if ent.bank == 3:
					#crystal shard
					if ent.id == 7:
						shards.append((area, ent, k))
					if ent.id == 9 or ent.id == 1:
						potentials.append((area, ent, k))
					if NoHealth:
						new = SwapThree(ent) #remove HP. Turn them into star bits
						if new:
							p2b(rom, new, (start + 0x2c *k + 2))
					#add ents randomly to randoms in case I need to update them to a crystal shard
					if random() > .85:
						randoms.append((area, ent, k))
				#mini boss
				if ent.bank == 8 and ent.id < 17:
					shards.append((area, ent, k))
				#big boss, doesn't actually appear in this search
				if ent.bank == 2:
					shards.append((area, ent, k))
					print('boss')
				#friend boss, no change
				if ent.bank == 1:
					shards.append((area, ent, k))
		extra = len(shards) - 3
		for j, s in enumerate(shards):
			#disable, this likely isn't the case, deal with later
			if extra and ent.id == 7 and ent.bank == 3:
					DisableShard(*s, rom)
					extra -= 1
			else:
				UpdateEep(*s, rom, j)
		#is this bad practice?
		if j < 2:
			#convert potential shards first, then use randoms
			potentials.extend(randoms)
			while(potentials):
				area = potentials[0][0]
				ptr = area.ptr + area.block.main_header[2] + 0x2c*potentials[0][2]
				p2b(rom, (3, 7), (ptr + 1))
				potentials.pop(0)
				j += 1
				if j == 2:
					break

def DisableShard(area, ent, k, rom):
	#turn the shard into a 1up
	ptr = area.ptr + area.block.main_header[2] + 0x2c*k
	rom.seek(ptr + 2)
	rom.write(struct.pack(">B", 9))

def UpdateEep(area, ent, k, rom, cur_shard):
	#only update crystal shards
	if (ent.bank, ent.id) == (3, 7):
		ptr = area.ptr + area.block.main_header[2] + 0x2c*k
		Wrld_lens = [2, 3, 3, 3, 3, 2, 0] #don't include boss stages
		total = sum( [a*3 for a in Wrld_lens[:area.areanew[0]]] ) #number of stages to get to cur world
		total += area.areanew[0] #add one more shard for bosses
		total += area.areanew[1]*3 #add in the shards for the prev stages in this world
		total += cur_shard #add in the shards for the cur level
		rom.seek(ptr + 6)
		rom.write(struct.pack(">B", total))

def scramble(sd):
	levels = []
	while(len(sd["first"])):
		num = int(len(sd["lvl"])/len(sd["first"]))
		levels.append(assemble(num,sd))
	return levels

def assemble(num, sd):
	f = int(random()*len(sd["first"]))
	lvl = [sd["first"][f]]
	sd["first"].pop(f)
	for i in range(num):
		a = int(random()*len(sd["lvl"]))
		lvl.append( sd["lvl"][a] )
		sd["lvl"].pop(a)
	b = int(random()*len(sd["carpet"]))
	lvl.append( sd["carpet"][b])
	sd["carpet"].pop(b)
	return lvl

def SetWarps(levels, rand, rom, sd, log):
	Wrld_lens = [2, 3, 3, 3, 3, 2, 0] #don't include boss stages
	New_SD = {}
	for k, lvl in enumerate(levels):
		#set first stage
		z = 0
		w = 0
		for i in range(7):
			#guess is ID of area 1 of actual level, add one so that we work only with normal stages, not boss stages
			guess = (i, k-z + 1, 0)
			if guess in sd.keys():
				break
			z += Wrld_lens[i]+1
			w += 1
		for j in range(len(lvl)):
			new_area = (w, k-z, j)
			#swap warps in cur and next level to link
			lvl[j].areanew = new_area
			if j != len(lvl)-1:
				SetWarp(lvl[j], lvl[j+1], rand, rom, sd, new_area)
			#set table in intended slot to be cur stage
			New_SD[new_area] = copy.deepcopy(lvl[j].stage)
			#change music in cur stage to valid ID
		#debug print to check all the forward warps, write to log
		
		a = [f"{l.area}: {l.set_warpF}" for l in lvl]
		b = [f"{l.area}: {l.set_warpB}" for l in lvl]
		q = [f"{l.areanew}" for l in lvl]

		log.Write("\n\nL:    "+" , ".join(q))
		log.Write("\nF:    "+" -> ".join(a))
		log.Write("\nB:    "+" <- ".join(b))
		
		
		#put boss level at end of world
		if k-z == Wrld_lens[w]:
			boss = (w, k-z + 1, 0)
			new_boss = sd["boss"][w]
			# sd["boss"].remove(new_boss)
			New_SD[boss] = new_boss.stage
			New_SD[boss].scram_music()
			log.Write(f"\nboss:    {new_boss.area}")
			# print(f"write boss to {boss}")
	return New_SD

def ScrambleMusic(New_SD):
	for stage in New_SD.values():
		stage.scram_music()

def defeatChkSum(Out):
	Out.seek(0x63c)
	[Out.write(b'\x00') for x in range(4)]
	Out.seek(0x648)
	[Out.write(b'\x00') for x in range(4)]
	return

def LogList(log, stages):
	for a in stages:
		f = f"Level {a.area} Entrance {a.entrance} Exit {a.exit}"
		log.Write(f)

def LogOrig(sd):
	log = Log()
	log.Write("Main Levels:")
	LogList(log, sd["lvl"])

def BuildStages(rom):
	MainStageTable = GetStages(rom)
	stageDict = {"lvl":[], 'waddleD':[], 'DDD':[], "carpet":[], "first":[], "boss":[]}
	for lvl, stage in MainStageTable.items():
		ptr = GetPointers(*stage.col, "Misc", rom)
		block = misc_bin(rom[ptr[0]:ptr[1]])
		Lvl = Level(block, stage, stageDict, ptr[0])
		#log stages for research
		#LogOrig(stageDict)
	return stageDict

def Main(romP, Seed = None, SpeedEdit = None, NoHealth = None, HardMode = None,
		ScramMusic = None):
	log = Log()
	if Seed:
		try:
			seed( eval(Seed) )
		except:
			print("invalid entry for seed")
	else:
		Seed = int(random()*0xFFFFFFFF)
		seed( Seed )
	#get data
	with open(romP,'rb') as rom:
		rom = rom.read()
		stageDict = BuildStages(rom)
		#scramble stages and get new level order
		levels = scramble(stageDict)
		rand_rom = Path(f'{romP.stem}_rand.z64')
		with open(rand_rom,'wb') as rand_rom:
			#copy rom
			rand_rom.write(rom)
			#place new warps so stages match ran order
			New_SD = SetWarps(levels, rand_rom, rom, stageDict, log)
			#scramble Music
			if ScramMusic:
				ScrambleMusic(New_SD)
			#re write stage table
			WriteStageTable(New_SD, rom, rand_rom)
			#scramble enemies
			ScrambleEnts(rand_rom, levels, log, NoHealth)
			#edit speeds
			if SpeedEdit:
				EditSpeed(rand_rom, levels)
			#remove chksum so game boots
			defeatChkSum(rand_rom)
	print("successfully randomized rom")
	return Seed

#test main
if  __name__ == "__main__":
	romP = Path('Kirby64.z64')
	Main(romP)