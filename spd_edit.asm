.n64
.open "kirby64_rand.z64", 0
.headersize 0x80057DB0

lvl_spds equ 0x800D5E00
cur_area equ 0x800be500
entry equ 0x800B6300
;this is a speed application function entry.
;speeds for ents are edited all over the place, likely
;in their entity specific code based on their specific action states

;I don't have access to their internal variables, just the spd table and the spd application
;func, which takes their prev spd, applies extra factors and then stores it to ent

;here I will multiply by a multiplier, and then store it in another table.
;I will then use logic to figure out when to multiply the speed based on the stored
;speeds, so that things behave somewhat normally


;replaces dev string space
.org 0x800C0540
;carry over from erased code in entry
add.s f8, f4, f6
add.s f10, f10, f8 ;f10 is cur speed

;my code
li at, ent_spd_table
addu at, at, v0 ;v0 is carried over from prev func to have cur ent offset
lwc1 f2, 0x0 (AT)
mtc1 r0, f4
c.eq.s f4, f2 ;if stored spd is zero, store cur spd
nop
bc1t store_spd
nop
c.eq.s f10, f4 ;if cur ent spd is zero, do nothing
nop
bc1t store_spd
abs.s f4, f10 ;use abs for comparisons

;table + world*48 + lvl *10 + area*4
li at, cur_area

lw t2, 0x0 (AT)
li t0, 48
multu t2, t0
mflo t1

lw t2, 0x4 (AT)
li t0, 10
multu t2, t0
mflo t2
addu t1, t2, t1

lw t2, 0x8 (AT)
li t0, 4
multu t2, t0
mflo t2
addu t2, t2, t1

li at, lvl_spds
addu at, at, t2
lwc1 f6, 0x0 (AT) ;spd multiplier

mul.s f8, f4, f6 ;cur * multiplier
c.eq.s f8, f2
nop
bc1t return ;if multiplier*spd == stored, then somehow doubled speed got stored, don't increase so no runaway happens
nop
c.lt.s f4, f2
nop
bc1t store_spd ;if spd < stored, ent is slowing down, don't mess with physics or it'll never be able to slow down
nop
mul.s f12, f6, f2 ;stored*multiplier
c.le.s f8, f12
nop
bc1t return ;if multiplier*stored == cur, then spd wasn't updated, don't edit
nop
mul.s f16, f10, f6
j entry
nop

store_spd:
abs.s f2, f10
swc1 f2, 0x0 (AT)
return:
j entry
mov.s f16, f10




Kirb_SPD:
lw t9, 0x0 (A2)

;table + world*48 + lvl *10 + area*4
li at, cur_area

lw t2, 0x0 (AT)
li t0, 48
multu t2, t0
mflo t1

lw t2, 0x4 (AT)
li t0, 10
multu t2, t0
mflo t2
addu t1, t2, t1

lw t2, 0x8 (AT)
li t0, 4
multu t2, t0
mflo t2
addu t2, t2, t1

li at, lvl_spds
addu at, at, t2
lwc1 f6, 0x0 (AT) ;spd multiplier
mul.s f4, f6, f4
swc1 f4, 0x174 (A3)

lui at, 0x800E
j 0x800B5370
or t1, t9, r0




ent_spd_table:
;fill 64 ents worth of padding with 0
.area 4*64, 0
.endarea



;ent spd edit
.org 0x800B62F8
j 0x800C0540
NOP



;now edit kirbys speed
.org 0x800B5368
j Kirb_SPD
NOP







.close